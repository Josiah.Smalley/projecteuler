using Solutions.Framework;

namespace Solutions.Solutions0001.Solutions0001;

public class S0001MultiplesOf3Or5 : ISolution<int, long>
{
    public static IEnumerable<string> Description => new[]
    {
        "This program will calculate the sum of all natural numbers below a given value (exclusive)."
    };
    
    public long Solve(int maxValue)
    {
        var sum = 0L;

        for (var i = 3; i < maxValue; i++)
        {
            if (i % 3 == 0 || i % 5 == 0)
            {
                sum += i;
            }
        }

        return sum;
    }

    public static long SolveInteractive()
    {
        foreach (var s in Description)
        {
            Console.WriteLine(s);
        }

        int? maxValue = null;
        while (!maxValue.HasValue)
        {
            Console.WriteLine("Find the sum of numbers below:");
            var input = Console.ReadLine();
            if (!int.TryParse(input, out var intValue))
            {
                Console.WriteLine("Not a valid input. Try again.");
                continue;
            }

            maxValue = intValue;
        }

        return new S0001MultiplesOf3Or5().Solve(maxValue.Value);
    }
}