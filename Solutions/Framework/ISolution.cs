namespace Solutions.Framework;

public interface ISolution<out TResult>
{
    TResult Solve();
}

public interface ISolution<in TInput, out TResult>
{
    static abstract IEnumerable<string> Description { get; }
    TResult Solve(TInput input);
    static abstract TResult SolveInteractive();
}