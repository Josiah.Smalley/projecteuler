using Solutions.Solutions0001.Solutions0001;

namespace SolutionTests.Tests0001.Tests0001;

public class T0001MultiplesOf3Or5
{
    [Test]
    public void ExamplePasses()
    {
        const int maxValue = 10;
        const long expectedResult = 23L;
        var actualResult = new S0001MultiplesOf3Or5().Solve(maxValue);

        Assert.That(actualResult, Is.EqualTo(expectedResult));
    }
}